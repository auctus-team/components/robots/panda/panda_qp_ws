# panda_qp_ws

A workspace to test panda_qp_control

# Installation

We are assuming that ROS noetic is already installed on your computer.
We first need to make sure a couple of stuff are installed

```console
sudo apt update
sudo apt install python3-rosdep python3-catkin-tools build-essential cmake git libpoco-dev libeigen3-dev
pip install scipy
```

Then create the catkin workspace

```console
INSTALLATION_PATH=~/panda_qp_ws
mkdir -p $INSTALLATION_PATH
cd $INSTALLATION_PATH
git clone --recurse-submodules git@gitlab.inria.fr:auctus-team/components/robots/panda/panda_qp_ws.git src
catkin config --init --extend /opt/ros/noetic
rosdep install --from-paths src --ignore-src -r -y
catkin build
source devel/setup.bash
```

Note: you can add the last line to your `~/.bashrc` to avoid having to source it every time you open a new terminal.

# Usage

## In simulation 

For a velocity controller :
`roslaunch panda_qp_control velocity_control.launch sim:=true`

Note: The simulation of the robot in velocity is a bit hacky for now. It uses a EffortInterface. The joint velocity computed by the QP is fed to a proportionnal controller as $`k_p (\dot{q}^{opt} - \dot{q})`$. This is because for now the [franka_gazebo](https://frankaemika.github.io/docs/franka_ros.html#franka-gazebo) doesn't take into consideration a VelocityJointInterface. Hopefully this will be fixed with [https://github.com/frankaemika/franka_ros/pull/181](https://github.com/frankaemika/franka_ros/pull/181)

For a torque controller :
`roslaunch panda_qp_control torque_control.launch sim:=true`

## On the real robot

For a velocity controller :
`roslaunch velocity_qp velocity_control.launch robot_ip:=your_robot_ip`

Note: There is the same error as with the simulation. Therefore the robot is not stiffly actuated. This can be fixed by using a hardware_interface::VelocityJointInterface but it prevents simulating the robot.

For a torque controller :
`roslaunch panda_qp_control torque_control.launch robot_ip:=your_robot_ip`

## Run a trajectory

In this version of panda_qp_control, trajectories are computed using MoveIt. Several examples of trajectories are available in the package [moveit_trajectory_interface](https://gitlab.inria.fr/auctus-team/components/motion-planning/moveit_trajectory_interface)/test. 

To launch a trajectory simply run the command :
`rosrun moveit_trajectory_interface go_to_cartesian_pose.py`

MoveIt is used to allow a more high level definition of a trajectory than with [panda_traj](https://gitlab.inria.fr/auctus-team/components/control/panda_traj). MoveIt also include obstacle avoidance feature when planning for a motion. External sensors can also be added to sample the environment and take it into account durint the planning. The [moveit_trajectory_interface](https://gitlab.inria.fr/auctus-team/components/motion-planning/moveit_trajectory_interface) is a package that grabs a trajectory object planned by moveit and exposes the resulting trajectory as a function of time. A new point along the trajectory must be fed to the QP controller at each time step (1ms). More information on the trajectory generation can be found [here](https://gitlab.inria.fr/auctus-team/components/motion-planning/moveit_trajectory_interface))

If one wants to use the old way to generate trajectories, a branch with kdl_trajectories is available [here](https://gitlab.inria.fr/auctus-team/components/robots/panda/panda_qp_control/-/tree/kdl_trajectories)

## Optional roslaunch parameter : 

`robot_ip` IP adress of the panda robot

`sim` Run the code in simulation on Gazebo

`load_gripper` launch the panda model with its gripper

## Controller parameters

The controller parameters are stored in a yaml file in the `/config` folder and loaded as ros parameters in the `run.launch` file. 

